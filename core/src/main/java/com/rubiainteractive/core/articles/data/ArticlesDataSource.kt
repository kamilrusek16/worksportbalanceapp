package com.rubiainteractive.core.articles.data

import com.rubiainteractive.core.Response
import com.rubiainteractive.core.articles.domain.Article
import io.reactivex.Single

interface ArticlesDataSource {

    fun get(page: Int, objects: Int): Single<Response<List<Article>>>
    fun getSingle(id: Int): Single<Response<Article>>
}