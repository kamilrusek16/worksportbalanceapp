package com.rubiainteractive.core.articles.interactors

import com.rubiainteractive.core.articles.data.ArticlesRepository

class GetArticles(private val repository: ArticlesRepository) {

    public fun execute(page: Int, objects: Int) = repository.getArticles(page, objects)
}