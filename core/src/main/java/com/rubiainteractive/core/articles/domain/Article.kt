package com.rubiainteractive.core.articles.domain

import java.io.Serializable
import java.util.*

data class Article(val id: Int,
                   val title: String?,
                   val content: String?,
                   val description: String?,
                   val author: String?,
                   val date: Date,
                   val imageUrl: String?) : Serializable