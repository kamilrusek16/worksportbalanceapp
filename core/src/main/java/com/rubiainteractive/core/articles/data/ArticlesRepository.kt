package com.rubiainteractive.core.articles.data

class ArticlesRepository constructor(private val articlesDataSource: ArticlesDataSource) {

    fun getArticles(page: Int, objects: Int) = articlesDataSource.get(page, objects)

    fun getSingleArticle(id: Int) = articlesDataSource.getSingle(id)
}