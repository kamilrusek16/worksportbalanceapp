package com.rubiainteractive.core.articles.interactors

import com.rubiainteractive.core.articles.data.ArticlesRepository

class GetSingleArticle(private val repository: ArticlesRepository) {

    fun execute(id: Int) = repository.getSingleArticle(id)
}