package com.rubiainteractive.core

data class Response<T>(val status: Int, val count: Int, val content: T)