package com.rubiainteractive.app.secure;

import android.util.Base64;

public class KeysProvider {

    static {
        System.loadLibrary("keys");
    }

    public native String getNewsKey();

    public String getNewsApiKey() {
        return new String(Base64.decode(getNewsKey(),Base64.DEFAULT));
    }
}
