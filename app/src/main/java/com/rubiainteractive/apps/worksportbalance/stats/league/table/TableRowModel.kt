package com.rubiainteractive.apps.worksportbalance.stats.league.table

class TableRowModel(val tableTeam: TableTeam, val tableConfig: TableConfig)