package com.rubiainteractive.apps.worksportbalance.stats.league.table

import android.content.Context
import android.view.LayoutInflater
import android.widget.TableRow
import androidx.databinding.DataBindingUtil
import com.rubiainteractive.apps.worksportbalance.R
import com.rubiainteractive.apps.worksportbalance.databinding.TableTeamRowBinding

class TableTeamRow(context: Context, tableRowModel: TableRowModel) {

    val tableRow: TableRow

    init {

        val inflater = LayoutInflater.from(context)
        val binding = DataBindingUtil.inflate<TableTeamRowBinding>(inflater, R.layout.table_team_row, null, false)
        binding.tableRowModel = tableRowModel

        tableRow = binding.root as TableRow
    }
}