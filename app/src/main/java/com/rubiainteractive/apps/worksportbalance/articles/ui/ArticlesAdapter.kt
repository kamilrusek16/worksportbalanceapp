package com.rubiainteractive.apps.worksportbalance.articles.ui

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.paging.PagedListAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.rubiainteractive.apps.worksportbalance.R
import com.rubiainteractive.apps.worksportbalance.databinding.ArticleRowBinding
import com.rubiainteractive.core.articles.domain.Article
import com.squareup.picasso.Picasso

class ArticlesAdapter(private val onArticleClick: OnArticleClick) : PagedListAdapter<Article, ArticlesAdapter.ViewHolder>(
    ArticlesDiffCallback) {

    interface OnArticleClick {
        fun onClick(article: Article)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val binding = DataBindingUtil.inflate<ArticleRowBinding>(inflater, R.layout.article_row, parent, false)

        return ViewHolder(binding, onArticleClick)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(getItem(position)!!)
    }

    class ViewHolder(private val binding: ArticleRowBinding, private val onArticleClick: OnArticleClick) : RecyclerView.ViewHolder(binding.root) {
        fun bind(article: Article) {

            binding.root.setOnClickListener {
                val position = adapterPosition

                if(position != RecyclerView.NO_POSITION)
                    onArticleClick.onClick(article)
            }

            binding.article = article
            Picasso.get().load(article.imageUrl).placeholder(R.drawable.ic_worksportbalance).into(binding.ivArticle)
            binding.executePendingBindings()
        }
    }

    companion object {
        val ArticlesDiffCallback = object : DiffUtil.ItemCallback<Article>() {
            override fun areItemsTheSame(oldItem: Article, newItem: Article) = oldItem.id == newItem.id
            override fun areContentsTheSame(oldItem: Article, newItem: Article) = oldItem == newItem
        }
    }
}