package com.rubiainteractive.apps.worksportbalance.articles.data

import com.rubiainteractive.data.articles.ApiResponse
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Query

interface ArticlesApi {

    @GET(ArticlesApiConfig.articles)
    fun getArticles(@Query("page") page: Int, @Query("pageSize") pageSize: Int, @Query("apiKey") apiKey: String): Single<ApiResponse>
}