package com.rubiainteractive.apps.worksportbalance.stats.league.table

import androidx.lifecycle.ViewModel

class TableTeamViewModel : ViewModel() {

    private val tableTeamsMock = arrayOf(
        TableTeam(1, 1, null, "Manchester United", 10, 30, 10, 0, 0, 25, 1, "WWWWW"),
        TableTeam(2, 2, null, "Manchester City", 10, 24, 7, 3, 0, 22, 5, "LDLWW"),
        TableTeam(3, 3, null, "Chelsea", 10, 18, 5, 3, 2, 15, 10, "WWWLL"),
        TableTeam(4, 4, null, "Arsenal", 10, 18, 5, 3, 2, 10, 8, "DDLWW"),
        TableTeam(5, 5, null, "Tottenham", 10, 18, 5, 3, 2, 10, 10, "DDDWL"),
        TableTeam(6, 6, null, "West Ham United", 10, 20, 5, 5, 0, 10, 15, "LDL"),
        TableTeam(7, 7, null, "Sheffield United", 10, 20, 5, 5, 0, 10, 15, "LDL"),
        TableTeam(8, 8, null, "Aston Villa", 10, 20, 5, 5, 0, 10, 15, "LDL"),
        TableTeam(9, 9, null, "Southampton", 10, 20, 5, 5, 0, 10, 15, "LDL"),
        TableTeam(10, 10, null, "Crystal Palace", 10, 20, 5, 5, 0, 10, 15, "LDL"),
        TableTeam(11, 11, null, "Leicester", 10, 20, 5, 5, 0, 10, 15, "LDL"),
        TableTeam(12, 12, null, "Everton", 10, 20, 5, 5, 0, 10, 15, "LDL"),
        TableTeam(13, 13, null, "Brighton", 10, 20, 5, 5, 0, 10, 15, "LDL"),
        TableTeam(14, 14, null, "Wolverhampton", 10, 20, 5, 5, 0, 10, 15, "LDL"),
        TableTeam(15, 15, null, "Watford", 10, 20, 5, 5, 0, 10, 15, "LDL"),
        TableTeam(16, 16, null, "Norwich City", 10, 20, 5, 5, 0, 10, 15, "LDL"),
        TableTeam(17, 17, null, "Newcastle United", 10, 20, 5, 5, 0, 10, 15, "LDL"),
        TableTeam(18, 18, null, "Bornemouth", 10, 20, 5, 5, 0, 10, 15, "LDL"),
        TableTeam(19, 19, null, "Burnley", 10, 20, 5, 5, 0, 10, 15, "LDL"),
        TableTeam(320, 20, null, "Live", 10, 0, 0, 0, 10, 1, 25, "LLL")
    )

    val tableConfig = TableConfig(showGoals = false , showForm = false)

    fun getTableRows() = tableTeamsMock.map {
        TableRowModel(it, tableConfig)
    }
}
