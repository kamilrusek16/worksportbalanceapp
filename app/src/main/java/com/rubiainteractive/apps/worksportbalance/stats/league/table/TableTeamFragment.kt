package com.rubiainteractive.apps.worksportbalance.stats.league.table

import androidx.lifecycle.ViewModelProviders
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import com.rubiainteractive.apps.worksportbalance.R
import com.rubiainteractive.apps.worksportbalance.databinding.TableTeamFragmentBinding
import kotlinx.android.synthetic.main.table_team_fragment.*

class TableTeamFragment : Fragment() {

    companion object {
        fun newInstance() = TableTeamFragment()
    }

    private lateinit var viewModel: TableTeamViewModel
    private lateinit var binding: TableTeamFragmentBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = TableTeamFragmentBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProviders.of(this).get(TableTeamViewModel::class.java)
        binding.tableConfig = viewModel.tableConfig
        table_view.build(viewModel.getTableRows().toTypedArray())
    }

}
