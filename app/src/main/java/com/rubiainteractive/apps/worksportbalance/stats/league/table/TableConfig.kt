package com.rubiainteractive.apps.worksportbalance.stats.league.table

import androidx.databinding.BaseObservable
import androidx.databinding.Bindable
import androidx.databinding.library.baseAdapters.BR

class TableConfig(private var showGoals: Boolean, private var showForm: Boolean) : BaseObservable() {

    @Bindable
    fun getShowGoals(): Boolean {
        return showGoals
    }
    fun setShowGoals(value: Boolean) {

        if(value != showGoals) {
            showGoals = value
            notifyPropertyChanged(BR.showGoals)
            notifyPropertyChanged(BR.showTeamName)
        }
    }

    @Bindable
    fun getShowForm(): Boolean {
        return showForm
    }
    fun setShowForm(value: Boolean) {
        if(value != showForm) {
            showForm = value
            notifyPropertyChanged(BR.showForm)
            notifyPropertyChanged(BR.showTeamName)
        }
    }

    val showTeamName
        @Bindable get() = !showGoals || !showForm
}