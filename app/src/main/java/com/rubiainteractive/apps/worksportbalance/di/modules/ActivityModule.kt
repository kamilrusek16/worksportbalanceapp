package com.rubiainteractive.apps.worksportbalance.di.modules

import com.rubiainteractive.apps.worksportbalance.articles.ui.ArticlesFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class ActivityModule {

    @ContributesAndroidInjector
    abstract fun inject(): ArticlesFragment
}