package com.rubiainteractive.apps.worksportbalance.articles.data

import com.rubiainteractive.app.secure.KeysProvider

class ArticlesApiConfig {

    companion object {
        const val apiUrl = "https://newsapi.org/"
        const val articles = "v2/everything?q=football"
        val keys = KeysProvider()
    }
}