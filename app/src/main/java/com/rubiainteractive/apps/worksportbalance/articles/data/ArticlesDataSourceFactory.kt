package com.rubiainteractive.apps.worksportbalance.articles.data

import androidx.lifecycle.MutableLiveData
import androidx.paging.DataSource
import com.rubiainteractive.core.articles.data.ArticlesRepository
import com.rubiainteractive.core.articles.domain.Article
import com.rubiainteractive.core.articles.interactors.GetArticles
import io.reactivex.disposables.CompositeDisposable


class ArticlesDataSourceFactory(private val compositeDisposable: CompositeDisposable,
                                private val getArticles: GetArticles) : DataSource.Factory<Int, Article>() {

    val liveData = MutableLiveData<ArticlesDataSource>()

    override fun create(): DataSource<Int, Article> {
        val articlesDataSource = ArticlesDataSource(getArticles, compositeDisposable)
        liveData.postValue(articlesDataSource)
        return articlesDataSource
    }
}