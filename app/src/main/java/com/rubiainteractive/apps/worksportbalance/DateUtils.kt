package com.rubiainteractive.apps.worksportbalance

import android.annotation.SuppressLint
import java.text.SimpleDateFormat
import java.util.*

class DateUtils {

    companion object {
        @JvmStatic
        @SuppressLint("SimpleDateFormat")
        fun toFriendlyFormat(date: Date) = SimpleDateFormat("yyyy-MM-dd HH:mm").format(date)
    }
}