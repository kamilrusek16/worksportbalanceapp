package com.rubiainteractive.apps.worksportbalance.di.modules

import dagger.Module
import dagger.Provides
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import timber.log.Timber
import javax.inject.Singleton

@Module
class NetworkModule {

    @Singleton
    @Provides
    fun provideLoggingInterceptor(): HttpLoggingInterceptor {
        val logging = HttpLoggingInterceptor(object: HttpLoggingInterceptor.Logger {
            override fun log(message: String) {
                Timber.e(message)
            }

        })
        logging.setLevel(HttpLoggingInterceptor.Level.BASIC)

        return logging
    }

    @Singleton
    @Provides
    fun provideInterceptors(loggingInterceptor: HttpLoggingInterceptor): List<Interceptor> {

        return arrayListOf(
            loggingInterceptor
        )
    }

    @Singleton
    @Provides
    fun provideOkHttpClientBuilder(loggingInterceptor: HttpLoggingInterceptor): OkHttpClient.Builder {

        val builder = OkHttpClient.Builder()

        //interceptors.forEach {
            builder.addInterceptor(loggingInterceptor)
        //}

        return builder
    }

    @Singleton
    @Provides
    fun provideRetrofit(okHttpClientBuilder: OkHttpClient.Builder) =
        Retrofit.Builder()
            .baseUrl("https://newsapi.org/")
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .addConverterFactory(GsonConverterFactory.create())
            .client(okHttpClientBuilder.build())
            .build()
}