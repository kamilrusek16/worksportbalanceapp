package com.rubiainteractive.apps.worksportbalance.articles.ui.single

import androidx.lifecycle.ViewModelProviders
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.navigation.fragment.navArgs

import com.rubiainteractive.apps.worksportbalance.R
import com.rubiainteractive.apps.worksportbalance.databinding.SingleArticleFragmentBinding
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.single_article_fragment.*

class SingleArticleFragment : Fragment() {


    private lateinit var viewModel: SingleArticleViewModel
    private lateinit var binding: SingleArticleFragmentBinding
    val args: SingleArticleFragmentArgs by navArgs()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.single_article_fragment, container, false)
        binding.article = args.article
        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProviders.of(this).get(SingleArticleViewModel::class.java)
        Picasso.get().load(args.article.imageUrl).placeholder(R.drawable.ic_worksportbalance).into(iv_single_article_image)
    }

}
