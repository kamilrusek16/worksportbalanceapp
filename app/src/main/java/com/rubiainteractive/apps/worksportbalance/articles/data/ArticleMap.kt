package com.rubiainteractive.apps.worksportbalance.articles.data

import com.rubiainteractive.data.articles.Article

fun Article.toDomain(): com.rubiainteractive.core.articles.domain.Article {
    return com.rubiainteractive.core.articles.domain.Article(
        0,
        this.title,
        this.content,
        this.description,
        this.author,
        this.publishedAt,
        this.urlToImage
    )
}