package com.rubiainteractive.apps.worksportbalance.articles.ui

import androidx.lifecycle.LiveData
import androidx.lifecycle.Transformations
import androidx.lifecycle.ViewModel
import androidx.paging.LivePagedListBuilder
import androidx.paging.PagedList
import com.rubiainteractive.apps.worksportbalance.articles.data.ArticlesDataSource
import com.rubiainteractive.apps.worksportbalance.articles.data.ArticlesDataSourceFactory
import com.rubiainteractive.core.articles.domain.Article
import com.rubiainteractive.core.articles.interactors.GetArticles
import com.rubiainteractive.data.articles.RequestState
import io.reactivex.disposables.CompositeDisposable


class ArticlesViewModel(getArticles: GetArticles) : ViewModel() {

    private val disposables: CompositeDisposable = CompositeDisposable()
    private val dataSourceFactory = ArticlesDataSourceFactory(disposables, getArticles)

    val requestLiveState: LiveData<RequestState>
    val articlesLiveData: LiveData<PagedList<Article>>

    init {
        val pagedListConfig = PagedList.Config.Builder()
            .setEnablePlaceholders(true)
            .setInitialLoadSizeHint(10)
            .setPageSize(10).build()

        articlesLiveData = LivePagedListBuilder(dataSourceFactory, pagedListConfig).build()
        requestLiveState = Transformations.switchMap(dataSourceFactory.liveData, ArticlesDataSource::requestLiveState)
    }

    override fun onCleared() {
        super.onCleared()
        disposables.clear()
    }
}
