package com.rubiainteractive.apps.worksportbalance.stats.league.table

enum class FormState {
    Win,
    Lost,
    Draw
}