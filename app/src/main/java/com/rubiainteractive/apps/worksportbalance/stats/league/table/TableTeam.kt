package com.rubiainteractive.apps.worksportbalance.stats.league.table

data class TableTeam(val id: Int, val position:Int, val logoUrl: String?, val name: String,
                     val played: Int, val points: Int,
                     val wins: Int, val draws: Int, val losses: Int,
                     val goalsFor:Int, val goalsAgainst: Int, val form: String)