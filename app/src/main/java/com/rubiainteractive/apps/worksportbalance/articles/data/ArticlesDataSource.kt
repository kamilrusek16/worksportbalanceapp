package com.rubiainteractive.apps.worksportbalance.articles.data

import androidx.lifecycle.MutableLiveData
import androidx.paging.PageKeyedDataSource
import com.rubiainteractive.core.articles.domain.Article
import com.rubiainteractive.core.articles.interactors.GetArticles
import com.rubiainteractive.data.articles.RequestState
import io.reactivex.disposables.CompositeDisposable

class ArticlesDataSource(private val getArticles: GetArticles,
                         private val disposables: CompositeDisposable) : PageKeyedDataSource<Int, Article>() {

    private var page = 1

    val requestLiveState = MutableLiveData<RequestState>()

    override fun loadInitial(
        params: LoadInitialParams<Int>,
        callback: LoadInitialCallback<Int, Article>
    ) {
        disposables.add(
            getArticles.execute(page, params.requestedLoadSize)
                .doOnSubscribe {
                    requestLiveState.postValue(RequestState.Pending)
                }
                .subscribe({

                    if(it.status == 200) {

                        requestLiveState.postValue(RequestState.Successful)

                        callback.onResult(it.content, null, page + 1)
                    } else {
                        requestLiveState.postValue(RequestState.Failure)
                    }

                }, {
                    requestLiveState.postValue(RequestState.Failure)
                })
        )
    }

    override fun loadAfter(params: LoadParams<Int>, callback: LoadCallback<Int, Article>) {

        disposables.add(
            getArticles.execute(params.key, params.requestedLoadSize)
                .doOnSubscribe {
                    requestLiveState.postValue(RequestState.Pending)
                }
                .subscribe({

                    if(it.status == 200) {

                        val pagesCount = (it.count/params.requestedLoadSize) + 1
                        requestLiveState.postValue(RequestState.Successful)

                        callback.onResult(it.content, if(params.key == pagesCount) null else params.key + 1)
                    } else {
                        requestLiveState.postValue(RequestState.Failure)
                    }

                }, {
                    requestLiveState.postValue(RequestState.Failure)
                })
        )
    }

    override fun loadBefore(params: LoadParams<Int>, callback: LoadCallback<Int, Article>) {}
}