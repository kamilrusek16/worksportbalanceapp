package com.rubiainteractive.apps.worksportbalance.articles.ui

import androidx.lifecycle.ViewModelProviders
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import com.rubiainteractive.apps.worksportbalance.R

class LeaguesFragment : Fragment() {

    companion object {
        fun newInstance() = LeaguesFragment()
    }

    private lateinit var viewModel: LeaguesViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.leagues_fragment, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProviders.of(this).get(LeaguesViewModel::class.java)
        // TODO: Use the ViewModel
    }

}
