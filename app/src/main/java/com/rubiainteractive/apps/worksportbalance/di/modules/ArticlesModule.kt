package com.rubiainteractive.apps.worksportbalance.di.modules

import com.rubiainteractive.apps.worksportbalance.articles.data.ArticlesApi
import com.rubiainteractive.apps.worksportbalance.articles.data.RetrofitArticlesDataSource
import com.rubiainteractive.core.articles.data.ArticlesDataSource
import com.rubiainteractive.core.articles.data.ArticlesRepository
import com.rubiainteractive.core.articles.interactors.GetArticles
import dagger.Binds
import dagger.Module
import dagger.Provides
import retrofit2.Retrofit
import javax.inject.Singleton

@Module
abstract class ArticlesModule {

    @Singleton
    @Binds
    abstract fun provideArticlesDataSource(datasource: RetrofitArticlesDataSource): ArticlesDataSource

    companion object {

        @Singleton
        @Provides
        @JvmStatic
        fun provideArticlesApi(retrofit: Retrofit): ArticlesApi {
            return retrofit.create(ArticlesApi::class.java)
        }

        @Singleton
        @Provides
        @JvmStatic
        fun provideArticlesRepository(articlesDataSource: ArticlesDataSource): ArticlesRepository {
            return ArticlesRepository(articlesDataSource)
        }

        @Singleton
        @Provides
        @JvmStatic
        fun provideGetArticlesUseCase(articlesRepository: ArticlesRepository): GetArticles {
            return GetArticles(articlesRepository)
        }
    }
}