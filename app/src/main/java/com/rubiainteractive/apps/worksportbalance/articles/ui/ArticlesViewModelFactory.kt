package com.rubiainteractive.apps.worksportbalance.articles.ui

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.rubiainteractive.core.articles.interactors.GetArticles
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class ArticlesViewModelFactory @Inject constructor(private val getArticles: GetArticles) : ViewModelProvider.Factory {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T
            = modelClass.getConstructor(GetArticles::class.java).newInstance(getArticles)
}