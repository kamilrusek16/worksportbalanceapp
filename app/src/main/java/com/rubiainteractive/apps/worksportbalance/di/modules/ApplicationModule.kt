package com.rubiainteractive.apps.worksportbalance.di.modules

import com.rubiainteractive.apps.worksportbalance.MainActivity
import com.rubiainteractive.apps.worksportbalance.di.scopes.ActivityScope
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class ApplicationModule {

    @ContributesAndroidInjector(modules = [ActivityModule::class])
    @ActivityScope
    abstract fun inject(): MainActivity
}