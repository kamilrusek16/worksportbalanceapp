package com.rubiainteractive.apps.worksportbalance.articles.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.findNavController
import androidx.paging.PagedList
import androidx.recyclerview.widget.LinearLayoutManager
import com.rubiainteractive.apps.worksportbalance.R
import com.rubiainteractive.apps.worksportbalance.articles.data.ArticlesApi
import com.rubiainteractive.apps.worksportbalance.articles.data.RetrofitArticlesDataSource
import com.rubiainteractive.core.articles.data.ArticlesRepository
import com.rubiainteractive.core.articles.domain.Article
import com.rubiainteractive.core.articles.interactors.GetArticles
import dagger.android.support.DaggerFragment
import kotlinx.android.synthetic.main.articles_fragment.*
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.util.*
import javax.inject.Inject

class ArticlesFragment : DaggerFragment() {

    private lateinit var viewModel: ArticlesViewModel

    @Inject
    lateinit var viewModelFactory: ArticlesViewModelFactory

    private lateinit var adapter: ArticlesAdapter

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.articles_fragment, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        viewModel = ViewModelProviders.of(this, viewModelFactory).get(ArticlesViewModel::class.java)

        //optimization purposes
        rl_articles.setHasFixedSize(true)
        rl_articles.layoutManager = LinearLayoutManager(context)

        adapter = ArticlesAdapter(object : ArticlesAdapter.OnArticleClick {
            override fun onClick(article: Article) {
                val action = ArticlesFragmentDirections.actionArticlesFragmentToSingleArticleFragment(article)
                findNavController().navigate(action)
            }
        })

        viewModel.articlesLiveData.observe(viewLifecycleOwner, androidx.lifecycle.Observer<PagedList<Article>> {
            adapter.submitList(it)
        })

        rl_articles.adapter = adapter
    }

}
