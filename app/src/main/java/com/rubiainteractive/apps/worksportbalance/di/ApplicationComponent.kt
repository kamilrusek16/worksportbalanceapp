package com.rubiainteractive.apps.worksportbalance.di

import com.rubiainteractive.apps.worksportbalance.ApplicationBase
import com.rubiainteractive.apps.worksportbalance.di.modules.ApplicationModule
import com.rubiainteractive.apps.worksportbalance.di.modules.ArticlesModule
import com.rubiainteractive.apps.worksportbalance.di.modules.NetworkModule
import dagger.Component
import dagger.android.AndroidInjector
import dagger.android.support.AndroidSupportInjectionModule
import javax.inject.Singleton

@Singleton
@Component(modules = [AndroidSupportInjectionModule::class,
                        ApplicationModule::class,
                        NetworkModule::class,
                        ArticlesModule::class])
interface ApplicationComponent : AndroidInjector<ApplicationBase> {

    @Component.Factory
    interface Factory : AndroidInjector.Factory<ApplicationBase>
}