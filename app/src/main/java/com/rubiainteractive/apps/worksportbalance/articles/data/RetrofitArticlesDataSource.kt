package com.rubiainteractive.apps.worksportbalance.articles.data

import com.rubiainteractive.core.Response
import com.rubiainteractive.core.articles.data.ArticlesDataSource
import com.rubiainteractive.core.articles.domain.Article
import io.reactivex.Single
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class RetrofitArticlesDataSource @Inject constructor(private val api: ArticlesApi) : ArticlesDataSource {

    override fun get(page: Int, objects: Int): Single<Response<List<Article>>>
            = api.getArticles(page, objects, ArticlesApiConfig.keys.newsApiKey)
                .map {
                    Response(
                        mapStringResponseToCode(it.status),
                        it.totalResults,
                        it.articles.map { dataArticle -> dataArticle.toDomain() }
                    )
                }

    override fun getSingle(id: Int): Single<Response<Article>> {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    private fun mapStringResponseToCode(code: String): Int {
        return when(code) {
            "ok" -> 200
            else -> 404
        }
    }
}