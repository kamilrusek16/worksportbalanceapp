package com.rubiainteractive.apps.worksportbalance.stats.league.table

import android.content.Context
import android.util.AttributeSet
import android.widget.TableLayout
import kotlinx.android.synthetic.main.table_team_fragment.view.*

class TableView(context: Context?, attrs: AttributeSet?) : TableLayout(context, attrs) {

    fun build(tableTeams: Array<TableRowModel>) {
        tableTeams.forEach {
            this@TableView.table_view_teams.addView(
                TableTeamRow(context, it).tableRow
            )
        }
    }
}