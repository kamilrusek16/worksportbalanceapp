package com.rubiainteractive.apps.worksportbalance.utils.views

import android.view.View
import android.widget.RelativeLayout
import androidx.databinding.BindingAdapter

class LayoutBindings {

    companion object {

        @JvmStatic
        @BindingAdapter("android:layout_centerHorizontal")
        fun setCenterVertical(view: View?, isCenterVertical: Boolean) {
            val layoutParams = view!!.layoutParams as RelativeLayout.LayoutParams
            layoutParams.addRule(
                RelativeLayout.CENTER_IN_PARENT,
                if (isCenterVertical) RelativeLayout.TRUE else 0
            )
            view.layoutParams = layoutParams
        }
    }

}