package com.rubiainteractive.data.articles

enum class RequestState {
    Pending,
    Successful,
    Failure
}