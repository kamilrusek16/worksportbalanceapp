package com.rubiainteractive.data.articles

import java.util.*

data class Article(val author: String?,
                   val title: String?,
                   val description: String?,
                   val urlToImage: String?,
                   val publishedAt: Date,
                   val content: String?)