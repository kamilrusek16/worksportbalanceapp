package com.rubiainteractive.data.articles

data class ApiResponse(val status: String, val totalResults: Int, val articles: List<Article>)